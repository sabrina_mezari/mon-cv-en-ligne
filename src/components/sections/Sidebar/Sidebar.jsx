import React from "react";
import CV from "../../../images/mon-cv-tech.pdf"

import { 
  FaGitlab,
  FaLinkedinIn,
  FaReact,
  FaHtml5,
  FaCss3,
  FaNodeJs
} from "react-icons/fa";
import { 
  SiJavascript
} from "react-icons/si";
import profile from "../../../images/profile.jpg";

const socials = [
  {
    id: 1,
    icon: <FaGitlab />,
    link: "https://gitlab.com/sabrina_mezari",
  },
  {
    id: 2,
    icon: <FaLinkedinIn />,
    link: "https://linkedin.com/in/sabrina-mezari-027127124",
  }
];

const langageInfo = [
  {
    id: 1,
    icon: <FaReact />,
  },
  {
    id: 2,
    icon: <FaHtml5 />,
  },
  {
    id: 3,
    icon: <FaCss3 />,
  },
  {
    id: 4,
    icon: <SiJavascript />,
  },
  {
    id: 4,
    icon: <FaNodeJs />,
  }
];

const Sidebar = () => {
  return (
    <aside className="sticky top-0 bg-white md:mx-8 lg:mx-4 mb-8 p-6 shadow-md rounded-md -mt-40">
      <div className="w-24 h-24 rounded-md overflow-hidden mx-auto mb-5">
        <img src={profile} alt="shafiqhammad" className="w-full" />
      </div>
      <div className="text-center">
        <h1 className="text-xl text-gray-800 font-bold mb-1">Sabrina MEZARI</h1>
        <p className="text-sm text-gray-400 mb-3">
          Développeuse web REACT
        </p>
        <div className="text-start pt-4">
          <ul className="flex flex-wrap justify-center" style={{padding: "20px 10px 30px 10px"}}>
          {langageInfo.map((social, id) => (
            <SocialIcon social={social} key={id} />
          ))}
        </ul>
        </div>
        <a
          href={CV}
          className="inline-block mb-3 rounded bg-purple-600 text-center border-0 py-2 px-6 text-white leading-7 tracking-wide hover:bg-purple-800"
          download="CV_Sabrina-Mezari_developpeuse_REACT"
        >
          Mon CV en pdf
        </a>
        <ul className="flex flex-wrap justify-center" style={{padding: "20px 10px 30px 10px"}}>
          {socials.map((social, id) => (
            <SocialIcon social={social} key={id} />
          ))}
        </ul>
      </div>
      
    </aside>
  );
};

export default Sidebar;

const SocialIcon = (props) => {
  const { icon, link } = props.social;
  return (
    <li className="m-2">
      <a
        href={link}
        className="w-8 h-8 bg-purple-100 rounded text-purple-800 flex items-center justify-center hover:text-white hover:bg-purple-600"
      >
        {icon}
      </a>
    </li>
  );
};
