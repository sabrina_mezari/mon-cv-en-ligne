import React from "react";
import starlink from "../../../images/portfolio/starlink.jpeg";
import grossesseInfo from "../../../images/portfolio/grossesse-infos.jpeg";
import laMaisonJungle from "../../../images/portfolio/la-maison-jungle.jpg";
import user from "../../../images/portfolio/user.jpeg";

import PortfolioItem from "./PortfolioItem";

const portfolioData = [
  {
    id: 1,
    image: grossesseInfo,
    title: "Grossesse Infos",
    link: "http://grossesse-infos.sabrina.mezari.tech/",
    description:
      "Dans cette application, l'utilisatrice pourra avoir des informations sur sa grossesse en fonction du terme. Ce dernier sera calculé grâce aux informations entrées à l'aide d'un calendrier sur la page d'accueil. Une page sur l'accouchement permmettra de regarder des vidéos thématiques",
  },
  {
    id: 2,
    image: starlink,
    title: "Starlink-list",
    link: "http://starlink.sabrina.mezari.tech/",
    description:
      "Ce projet a été crée avec React et à l'aide de l'API de la liste des satellites Starlink Vous pourrez consulter et également réorganiser la liste de ces satellites avec plusieurs de leurs critères. Une carte du monde est aussi à disposition pour voir leur position.",
  },
  
  {
    id: 3,
    image: laMaisonJungle,
    title: "La maison jungle",
    link: "http://la-maison-jungle.sabrina.mezari.tech/",
    description:
      "Ce projet a été crée avec un cours d'openclassrooms. Il s'agit d'un site de vente de plantes. On peut ajouter des plantes au panier et vider son panier. L'utilisateur peut également séléctionner des types de plantes"
  },
  {
    id: 4,
    image: user,
    title: "Random user",
    link: "http://random-user.sabrina.mezari.tech/",
    description:
      "J'ai crée ce projet à partir de l'API randomuser.me/api . L'API permet de générer un utilisateur de façon aléatoire. On pourra également modifier le téléphone et/ou le mail de l'utilisateur"
  }
];

const Portfolio = () => {
  return (
    <section className="pb-10">
      <div className="flex flex-wrap md:px-4">
        {portfolioData.map((portfolio, id) => (
          <PortfolioItem portfolio={portfolio} key={id} />
        ))}
      </div>
    </section>
  );
};

export default Portfolio;
