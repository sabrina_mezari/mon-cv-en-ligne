import React from "react";

const About = () => {
  return (
    <section className="py-8">
      <div className="flex flex-wrap md:px-4">
        <div className="w-full">
          <div className="md:mx-4">
            <h3 className="text-2xl text-gray-800 font-bold mb-4">Qui je suis ?</h3>
            <p className="text-sm text-gray-400 leading-6 mb-3">
              Passionnée par l'univers du développement web et récemment reconvertie depuis le métier d'infirmière, je suis à la recherche d'une nouvelle opportunité en tant que développeuse web.
              </p>
            <p className="text-sm text-gray-400 leading-6 mb-3">
              Forte de ma capacité d'adaptation et de ma volonté d'apprendre continuellement, j'ai entrepris une transition professionnelle pour donner vie à ma passion pour la programmation.
            </p>
            <p className="text-sm text-gray-400 leading-6 mb-3">
              Mon expérience antérieure en tant qu'infirmière m'a permis de développer des compétences clés telles que la résolution de problèmes, la gestion du temps et le travail d'équipe, qui se révèlent être des atouts précieux dans le domaine du développement web.
            </p>
            <p className="text-sm text-gray-400 leading-6 mb-3">
              Je suis désormais prête à mettre à profit ma rigueur et ma détermination pour contribuer activement à des projets web innovants et stimulants.
            </p>
            <p className="text-sm text-gray-400 leading-6 mb-3">
            Je prends plaisir à analyser et trouver les solutions aux problèmes. Déterminée +++ et toujours de bonne humeur 😁😁😁 <br/>
            N’hésitez pas à me contacter
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
