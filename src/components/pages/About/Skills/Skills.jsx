import React from "react";
import SkillItem from "./SkillItem";

const skillData = [
  {
    id: 1,
    title: "Débuter avec REACT",
    percentage: "100%",
  },
  {
    id: 2,
    title: "Simplifier vous le CSS avec SASS",
    percentage: "100%",
  },
  {
    id: 3,
    title: "Utiliser git et github pour vos projets de développement",
    percentage: "100%",
  },
  {
    id: 4,
    title: "Apprenez à utilizer la ligne de commande dans un terminal",
    percentage: "100%",
  },
  {
    id: 5,
    title: "Apprenez à programmer avec Javascript",
    percentage: "76%",
  },
  {
    id: 6,
    title: "Apprenez à créer votre site web avec HTML5 et CSS3",
    percentage: "100%",
  },
  {
    id: 7,
    title: "Créer une application React complète",
    percentage: "50%",
  },
  {
    id: 8,
    title: "Comprenez le web",
    percentage: "100%",
  },
];

const Skills = () => {
  return (
    <div className="py-4">
      <div className="flex flex-wrap">
        <div className="w-full">
          <div className="md:mx-4">
            <h3 className="text-2xl text-gray-800 font-bold mb-4">Formation Openclassroom</h3>
          </div>
        </div>
        {skillData.map((skill, id) => (
          <SkillItem skill={skill} key={id} />
        ))}
      </div>
    </div>
  );
};

export default Skills;
