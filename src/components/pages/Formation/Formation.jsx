import React from "react";
import Skills from "../About/Skills/Skills";


const Formation = () => {
  return (
    <section className="pb-10">
      <div className="flex flex-wrap md:px-4">
        <div style={{ textAlign: "justify", padding: "20px 10px 20px 10px", marginLeft:"10px"}}>
          <p>Participation à une formation REACT créée par Quentin Rouault.<br/>Formation de 4 mois, suivi d’un coaching personnalisé pour faire aboutir le
          projet de reconversion.</p>
        </div>
        <Skills />
      </div>
    </section>
  );
};

export default Formation;
